export default [
	{
		provider_code: 'ACOSS',
		provider_slug: 'acoss',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'AFDB',
		provider_slug: 'afdb',
		maintainer: 'pdi'
	},
	{
		provider_code: 'AMECO',
		provider_slug: 'ameco',
		maintainer: 'cbenz',
		star: true
	},
	{
		provider_code: 'AQICN',
		provider_slug: 'aqicn',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'BCB',
		provider_slug: 'bcb',
		maintainer: 'bduye'
	},
	{
		provider_code: 'BCEAO',
		provider_slug: 'bceao',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'BDF',
		provider_slug: 'bdf',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'BEA',
		provider_slug: 'bea',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'BI',
		provider_slug: 'bi',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'BIS',
		provider_slug: 'bis',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'BLS',
		provider_slug: 'bls',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'BOC',
		provider_slug: 'boc',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'BOE',
		provider_slug: 'boe',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'BOJ',
		provider_slug: 'boj',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'BUBA',
		provider_slug: 'buba',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'CBO',
		provider_slug: 'cbo',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'CEPII',
		provider_slug: 'cepii',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'citymapper',
		provider_slug: 'citymapper',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'CSO',
		provider_slug: 'cso',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'DARES',
		provider_slug: 'dares',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'DESTATIS',
		provider_slug: 'destatis',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'DGDDI',
		provider_slug: 'dgddi',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'DREES',
		provider_slug: 'drees',
		legacy_pipeline: true,
		maintainer: 'cbenz'
	},
	{
		provider_code: 'EC',
		provider_slug: 'ec',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'ECB',
		provider_slug: 'ecb',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'EIA',
		provider_slug: 'eia',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'ENEDIS',
		provider_slug: 'enedis',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'ENTSOE',
		provider_slug: 'entsoe',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'ELSTAT',
		provider_slug: 'elstat',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'ESRI',
		provider_slug: 'esri',
		legacy_pipeline: true
	},
	{
		provider_code: 'Eurostat',
		provider_slug: 'eurostat',
		legacy_pipeline: true,
		maintainer: 'cbenz',
		star: true
	},
	{
		provider_code: 'FAO',
		provider_slug: 'fao',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'FED',
		provider_slug: 'fed',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'FH',
		provider_slug: 'fh',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'Google',
		provider_slug: 'google',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'FHFA',
		provider_slug: 'fhfa',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'Franc-zone',
		provider_slug: 'franc-zone',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'OAG',
		provider_slug: 'oag',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'GGDC',
		provider_slug: 'ggdc',
		legacy_pipeline: true
	},
	{
		provider_code: 'ILO',
		provider_slug: 'ilo',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'IMF',
		provider_slug: 'imf',
		maintainer: 'cbenz',
		star: true
	},
	{
		provider_code: 'INDEC',
		provider_slug: 'indec',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'INE-SPAIN',
		provider_slug: 'ine-spain',
		legacy_pipeline: true,
		maintainer: 'bduye'
	},
	{
		provider_code: 'INEGI',
		provider_slug: 'inegi',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'INEPT',
		provider_slug: 'inept',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'INSEE',
		provider_slug: 'insee',
		legacy_pipeline: true,
		maintainer: 'cbenz',
		star: true
	},
	{
		provider_code: 'ISM',
		provider_slug: 'ism',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'ISTAT',
		provider_slug: 'istat',
		legacy_pipeline: true,
		maintainer: 'cbenz'
	},
	{
		provider_code: 'JHU',
		provider_slug: 'jhu',
		legacy_pipeline: true,
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'JRC',
		provider_slug: 'jrc',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'LBMA',
		provider_slug: 'lbma',
		legacy_pipeline: true
	},
	{
		provider_code: 'METEOFRANCE',
		provider_slug: 'meteofrance',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'METI',
		provider_slug: 'meti',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'MOSPI',
		provider_slug: 'mospi',
		legacy_pipeline: true,
		maintainer: 'bduye'
	},
	{
		provider_code: 'NAR',
		provider_slug: 'nar',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'NBB',
		provider_slug: 'nbb',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'ND_GAIN',
		provider_slug: 'nd_gain',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'NBS',
		provider_slug: 'nbs',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'OECD',
		provider_slug: 'oecd',
		legacy_pipeline: true,
		maintainer: 'cbenz',
		star: true
	},
	{
		provider_code: 'ONS',
		provider_slug: 'ons',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'OpenTable',
		provider_slug: 'opentable',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'oppins',
		provider_slug: 'oppins',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'pole-emploi',
		provider_slug: 'pole-emploi',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'RBA',
		provider_slug: 'rba',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'RBI',
		provider_slug: 'rbi',
		legacy_pipeline: true
	},
	{
		provider_code: 'ROSSTAT',
		provider_slug: 'rosstat',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'RTE',
		provider_slug: 'rte',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'SAFE',
		provider_slug: 'safe',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'SAIS-CARI',
		provider_slug: 'sais-cari',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'SAMA',
		provider_slug: 'sama',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'SARB',
		provider_slug: 'sarb',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'SCB',
		provider_slug: 'scb',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'SCSMICH',
		provider_slug: 'scsmich',
		maintainer: 'pdi'
	},
	{
		provider_code: 'SECO',
		provider_slug: 'seco',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'STATCAN',
		provider_slug: 'statcan',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'STATJP',
		provider_slug: 'statjp',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'STATPOL',
		provider_slug: 'statpol',
		legacy_pipeline: true,
		maintainer: 'bduye'
	},
	{
		provider_code: 'TCMB',
		provider_slug: 'tcmb',
		legacy_pipeline: true,
		maintainer: 'pdi'
	},
	{
		provider_code: 'UNCTAD',
		provider_slug: 'unctad',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'UNESCO',
		provider_slug: 'unesco',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'UNDP',
		provider_slug: 'undp',
		maintainer: 'MichelJuillard'
	},
	{
		provider_code: 'UNDATA',
		provider_slug: 'undata',
		legacy_pipeline: true,
		maintainer: 'pdi',
		star: true
	},
	{
		provider_code: 'WB',
		provider_slug: 'wb',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	},
	{
		provider_code: 'WTO',
		provider_slug: 'wto',
		legacy_pipeline: true,
		maintainer: 'bduye',
		star: true
	}
];
